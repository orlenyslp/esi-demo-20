package com.example.demo.inventory.application.service;

import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    public PlantInventoryEntry findEntryById(Long id) {
        return plantInventoryEntryRepository.findById(id).orElse(null);
    }

    public PlantInventoryItem findItemById(Long id) {
        return plantInventoryItemRepository.findById(id).orElse(null);
    }

    public CollectionModel<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate)     {
        List<PlantInventoryEntry> entries = inventoryRepository.findAvailablePlants(name, startDate, endDate);
        return plantInventoryEntryAssembler.toCollectionModel(entries);
    }

    public CollectionModel<PlantInventoryItemDTO> findAvailableItems(String name, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryItem> items = inventoryRepository.findAvailableItems(name, startDate, endDate);
        return plantInventoryItemAssembler.toCollectionModel(items);
    }

    public List<PlantInventoryItem> findAvailableItemsE(String name, LocalDate startDate, LocalDate endDate) {
        return inventoryRepository.findAvailableItems(name, startDate, endDate);
    }

}
